import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { UsuarioComponent } from './components/usuario/usuario.component';
import { RolComponent } from './components/rol/rol.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { ElasticService } from './services/elastic.service';
import { HttpClientModule } from '@angular/common/http';
import { TarjetasComponent } from './components/shared/tarjetas/tarjetas.component';
import { UsuariosComponent } from './components/usuarios/usuarios.component';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    UsuarioComponent,
    RolComponent,
    NavbarComponent,
    TarjetasComponent,
    UsuariosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
