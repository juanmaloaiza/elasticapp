import { Component, OnInit } from '@angular/core';
import { ElasticService } from '../../services/elastic.service';

@Component({
  selector: 'app-rol',
  templateUrl: './rol.component.html',
  styles: []
})
export class RolComponent implements OnInit {

  listaRoles: any[] = [];
  constructor(private servicio: ElasticService) {
    this.servicio.getRoles()
    .subscribe( (data: any) => {
      console.log(data);
      this.listaRoles = Object.keys(data).map(i => data[i]);
      console.log(this.listaRoles );
    },
    err => { console.log('Error'); }
    );
  }

  ngOnInit() {
  }

}
