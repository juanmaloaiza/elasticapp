import { Component, OnInit } from '@angular/core';
import { ElasticService } from '../../services/elastic.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styles: []
})
export class UsuarioComponent implements OnInit {

  constructor(private router: ActivatedRoute, private servicio: ElasticService) {
    this.router.params.subscribe(params => {
      console.log(params['id']);
      this.getUsuario(params['id']);
    });
  }

  getUsuario ( id: string) {
    this.servicio.getUsuario(id).subscribe(usuario => {
      console.log(usuario);
    });
  }


  ngOnInit() {
  }

}
