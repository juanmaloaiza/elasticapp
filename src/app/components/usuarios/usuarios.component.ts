import { Component, OnInit } from '@angular/core';
import { ElasticService } from '../../services/elastic.service';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styles: []
})
export class UsuariosComponent implements OnInit {

  listaUsuarios: any[] = [];

  constructor(private servicio: ElasticService) {
    this.servicio.getUsuarios()
    .subscribe( (data: any) => {
      console.log(data);
      this.listaUsuarios = Object.keys(data).map(i => data[i]);
      console.log(this.listaUsuarios );
    },
    err => { console.log('Error'); }
    );
  }

  ngOnInit() {
  }

}
