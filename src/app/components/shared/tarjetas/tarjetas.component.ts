import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tarjetas',
  templateUrl: './tarjetas.component.html',
  styles: []
})
export class TarjetasComponent implements OnInit {

  @Input() items: any[] = [];
  constructor( private router: Router) { }
  ngOnInit() {
  }

  verUsuario(item: any) {
    let username;
    username = item.username;
    console.log(username);
    this.router.navigate(['/usuario', username] );
  }

}
