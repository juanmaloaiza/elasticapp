import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';



@Injectable({
  providedIn: 'root'
})
export class ElasticService {

  constructor(private http: HttpClient) { console.log('Servicio de elastic listo');
  }

  getQuery (query: string)
  {
    const url = `http://localhost:9200/_security/${ query }`;
    const headers = new HttpHeaders({
      'Authorization': 'Basic ZWxhc3RpYzpWVkQ3ZExUY1B4ZXNBVGwxZEgyYw=='

    });
    return this.http.get(url, { headers });
  }

  getUsuarios() { return this.getQuery('user/');  }
  getRoles()  { return this.getQuery('role/');  }
  getUsuario(id: string)  { return this.getQuery(`user/${ id }`); }

}
